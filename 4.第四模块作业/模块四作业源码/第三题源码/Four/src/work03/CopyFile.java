package work03;

import java.io.*;

public class CopyFile {
    public static void copyFile(String oldPath,String newPath) {
    //使用缓冲流复制 提高效率
        BufferedInputStream bis=null;
        BufferedOutputStream bos=null;
        try {
            bis=new BufferedInputStream(new FileInputStream(oldPath));
            bos=new BufferedOutputStream(new FileOutputStream(newPath));
            byte[] arr=new byte[8*1024];
            int flag=0;
            while (bis.read(arr)!=-1) {
                bos.write(arr,0,bis.read(arr));
            }
            System.out.println("拷贝:"+oldPath+"--到--"+newPath+"成功了");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(bis!=null){
                try {
                    bis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(bos!=null){
                try {
                    bos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
