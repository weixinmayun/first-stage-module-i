package work03;

import java.io.File;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TheadPoolCopy {
    public static void main(String[] args) {
        String oldPath="E:\\source";
        String newPath="E:\\target";
        copyFileDir(oldPath,newPath);
    }
    public static void copyFileDir(String oldPath,String newPath){
        File sourceFolder=new File(oldPath);
        File targetFolder=new File(newPath);
        if(!sourceFolder.exists()){
            System.out.println("要复制的文件夹不存在");
            return;
        }
        if(!targetFolder.exists()){
            targetFolder.mkdirs();
        }
        ExecutorService executorService= Executors.newFixedThreadPool(10);
        for(File item:sourceFolder.listFiles()){
        //创建线程池
        executorService.submit(()->{
            if(item.isFile()){
                CopyFile.copyFile(item.getAbsolutePath(),targetFolder.getAbsolutePath()+"/"+item.getName());
            }
            else {
                TheadPoolCopy.copyFileDir(item.getAbsolutePath(),targetFolder.getAbsolutePath()+"/"+item.getName());
            }
             return null;
        });
        }
        executorService.shutdown();
    }
}
