package work05;

import org.json.JSONException;
import org.json.JSONObject;
import work04.UserMessage;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Server {
    private ServerSocket ssk=null;//监听Socket
    private Socket sk=null;//连接Socket
    List<Socket> socketList = new ArrayList<>();
    private String strPath="C:\\server";
    //构造函数初始化
    public Server(int port) {
        try {
            ssk = new ServerSocket(port);//初始化监听端口
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            strPath = CreateFolders.createFolders("C:\\server");//如果服务端没有放文件的文件夹 创建一个
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    //服务端main方法 主程序
    public static void main(String[] args) {
        Server server = new Server(8888); //传入端口初始化  并且构造方法内初始化监听Socket
        server.startWork();
    }
    //监听连接客户端 和连接客户端
    public void startWork() {

        sk = new Socket();
        while (true) {
            try {
                System.out.println("等待客户端连接......");
                // 当没有客户端连接时，则服务器阻塞在accept方法的调用这里
                sk = ssk.accept();
                //将连接的客户端Socket记录到集合中
                socketList.add(sk);
                System.out.println("客户端" + sk.getInetAddress() + "连接成功！");
                // 每当有一个客户端连接成功，则需要启动一个新的线程为之服务
                serverThread(sk);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {

            }


        }

    }
    //因有可能多个客户端 定义线程方法  接收到聊天内容和文件 转发到客户端
    public void serverThread(Socket sk) {
        new Thread(() -> {
            // 定义输入输出流流
            DataInputStream in = null;
            DataOutputStream out = null;
            try {
                in = new DataInputStream(sk.getInputStream());
                out = new DataOutputStream(sk.getOutputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }
            while (true) {
                try {
                       String strJson = in.readUTF().replace("\\","/");
                        JSONObject jsonObject = new JSONObject(strJson);
                        // 打印输出json对象
                        System.out.println(jsonObject);
                        String clientName =jsonObject.get("clientName").toString();
                        String type= jsonObject.get("sendType").toString();
                        String message =jsonObject.get("messageInfo").toString();
                        System.out.println(clientName+":"+type+":"+message);
                        for (Socket s : socketList) {
                            if(s!=sk) {
                                DataOutputStream oOsAll = new DataOutputStream(s.getOutputStream());
                                oOsAll.writeUTF(strJson);
                            }
                       }
                    if ("fileType".equals(type)) {
                        if ("bye".equals(type)||"bye".equals(message)) {
                            System.out.println("客户端" + sk.getInetAddress() + "已下线");
                            socketList.remove(sk);
                            sk.close();
                            break;
                        }
                        System.out.println("开始接收文件");
                        FileInputStream fis = null;
                        FileOutputStream fos = null;
                        fis = new FileInputStream(message);
                        File fileName=new File(message);
                        fos = new FileOutputStream(strPath+"//"+fileName.getName());
                        // 3.不断地从输入流中读取数据内容并写入到输出流中
                        System.out.println("服务端正在玩命接收中...");
                        byte[] bArr = new byte[1024];
                        int res = 0;
                        while ((res = fis.read(bArr)) != -1) {
                            fos.write(bArr, 0, res);
                        }
                        System.out.println("服务端接收文件成功！");
                        System.out.println("服务端开始转发了.....");
                    }
                } catch (IOException|JSONException e) {
                    e.printStackTrace();
                } finally {
                }
            }
        }).start();
    }

}
