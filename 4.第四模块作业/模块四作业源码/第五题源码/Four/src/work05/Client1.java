package work05;

import org.json.JSONException;
import org.json.JSONObject;
import work04.UserMessage;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Client1  {
    DataInputStream in = null;
    DataOutputStream out = null;
    private static Scanner sc;
    public String getUser() {
        return user;
    }
    public void setUser(String user) {
        this.user = user;
    }
    private String user;
    private String strPath;
    private static String sendType="";//默认是输入的是 (字符串) 其它情况为(文件)
    public Client1(String user, Socket socket){
        setUser(user);
        try {
            strPath=CreateFolders.createFolders("C:\\"+getUser());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        try {
            //初始化 定义输入输出流
            in=new DataInputStream(socket.getInputStream());
            out=new DataOutputStream(socket.getOutputStream());
            sc=new Scanner(System.in);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    public static void main(String[] args) {
        Socket socket=null;
        Client1 client=null;
        try {
            //建立连接
            socket=new Socket("127.0.0.1",8888);
            System.out.println(socket);
            //创建客户端对象
            client=new Client1("user1",socket);
            System.out.println("请输入要操作的类型:第一种:字符(strType)  第二种:文件(fileType)");
            String  scType=sc.next();
            sendType=scType;
            //开始通信
            Thread send=client.send();
            Thread receive =client.receive();
            send.start();
            receive.start();
            receive.join();
            send.join();
            client.closeConnect();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            client.closeConnect();
            if(socket!=null){
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    //创建线程发送信息
    public  Thread send(){
        Thread s=new Thread(()->{
            System.out.println("等陆成功");
            while (true){

                if(sendType.equals("strType")){
                    try {
                        System.out.println("请输入内容");
                        String scStr=sc.next();
                        String strJson = "{'clientName':"+user+", 'sendType':"+sendType+", 'messagelen':"+String.valueOf(scStr.length())+",'messageInfo':'"+scStr+"'}";
                        out.writeUTF(strJson);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                else if(sendType.equals("fileType")) {
                    System.out.println("请输入文件路径");
                    String scPath=sc.next();
                    File fileDir=new File(scPath);
                    if(!fileDir.exists()){
                        System.out.println("该文件不存在！请选择存在的文件路径");
                        continue;
                    }
                    try {
                        String strJson = "{'clientName':"+user+", 'sendType':"+sendType+", 'messagelen':"+String.valueOf(scPath.length())+",'messageInfo':'"+scPath+"'}";
                        out.writeUTF(strJson);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        return  s;
    }
    //创建线程接收信息
    public  Thread receive(){
        Thread r=new Thread(()->{
            while (true){
                String str="";
                try {
                    String strJson = in.readUTF().replace("\\","/");
                    JSONObject jsonObject = new JSONObject(strJson);
                    String type= jsonObject.get("sendType").toString();
                    String message =jsonObject.get("messageInfo").toString();
                    if("bye".equals(message)){
                        break;
                    }
                    //如果是文件 文件下载到客户端的文件下
                    if("fileType".equals(type)){
                        System.out.println("开始接收文件");
                        FileInputStream fis = null;
                        FileOutputStream fos = null;
                        fis = new FileInputStream(message);
                        File fileName=new File(message);
                        fos = new FileOutputStream(strPath+"\\"+fileName.getName());
                        // 3.不断地从输入流中读取数据内容并写入到输出流中
                        System.out.println("服务端正在玩命接收...");
                        byte[] bArr = new byte[1024];
                        int res = 0;
                        while ((res = fis.read(bArr)) != -1) {
                            fos.write(bArr, 0, res);
                        }
                        System.out.println("服务端接收文件成功！");
                    }
                    //不论文件还是字符内容 客户端都会收到提示信息
                    System.out.println("客户端收到服务端"+jsonObject+"转发来的信息");
                } catch (IOException |  JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        return  r;
    }
    //因在main方法static 不能直接调用非静态全局变量 写个方法调用
    public  void  closeConnect(){
        if(in!=null){
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if(out!=null){
            try {
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if(sc!=null){
            sc.close();
        }
    }

}
