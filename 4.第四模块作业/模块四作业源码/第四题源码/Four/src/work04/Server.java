package work04;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    public static void main(String[] args) {
        ServerSocket ssk=null; //监听
        Socket sk=null;//连接
        ObjectOutputStream oOs=null;
        ObjectInputStream oIs=null;
        try {
            //在8888端口创建Socket 监听
            ssk=new ServerSocket(8888);
            System.out.println("等待连接......");
            //等待连接
            sk=ssk.accept();
            //获取客户端发来的信息
            oIs=new ObjectInputStream(sk.getInputStream());
            //流转换为对象
            UserMessage userMessage=(UserMessage)oIs.readObject();
            if(userMessage.getUser().getUserName().equals("admin")&&userMessage.getUser().getUserPwd().equals("123456")){
                userMessage.setStrType("success");
            }else {
                userMessage.setStrType("fail");
            }
            System.out.println(userMessage.getStrType());
            oOs=new ObjectOutputStream(sk.getOutputStream());
            //发出UserMessage 给连接的客户端
            oOs.writeObject(userMessage);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            if(ssk!=null){
                try {
                    ssk.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(sk!=null){
                try {
                    sk.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(oIs!=null){
                try {
                    oIs.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (oOs!=null){
                try {
                    oOs.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

}
