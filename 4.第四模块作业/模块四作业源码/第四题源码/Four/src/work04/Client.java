package work04;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class Client {
    public static void main(String[] args) {
        Socket sk=null;
        ObjectInputStream oIS=null;
        ObjectOutputStream oOs=null;
        try {
            sk=new Socket("127.0.0.2",8888);
            System.out.println("连接成功");
            //UserMessge对象
            UserMessage userMessage=new UserMessage("",new UserMessage.User("admin","123456"));
            //创建对象输出流
            oOs=new ObjectOutputStream(sk.getOutputStream());
            //客户端发送信息
            oOs.writeObject(userMessage);
            //客户端接收信息
            oIS=new ObjectInputStream(sk.getInputStream());
            //获取服务端返回的UserMessage对象
            userMessage=(UserMessage)oIS.readObject();
            //输出结果
            if(userMessage.getStrType().equals("success")){
                System.out.println("登录成功!");
            }else {
                System.out.println("登录失败!");
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            if(sk!=null) {
                try {
                    sk.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(oOs!=null){
                try {
                    oOs.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(oIS!=null){
                try {
                    oIS.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
