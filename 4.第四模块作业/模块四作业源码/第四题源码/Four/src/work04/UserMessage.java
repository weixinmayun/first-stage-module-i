package work04;

import java.io.Serializable;

public class UserMessage implements  Serializable {

    private static final long serialVersionUID = -6849610558595770678L;
    private String strType;
    private  User  user;
    public  UserMessage(String strType,User user){
        setStrType(strType);
        setUser(user);
    }
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getStrType() {
        return strType;
    }
    public void setStrType(String strType) {
        this.strType = strType;
    }

    public  static class   User implements Serializable {

        private static final long serialVersionUID = -8060668504010029117L;
        private String userName;
        private String userPwd;
        public  User(String userName,String userPwd){
            setUserName(userName);
            setUserPwd(userPwd);
        }
        public String getUserName() {
            return userName;
        }
        public void setUserName(String userName) {
            this.userName = userName;
        }
        public String getUserPwd() {
            return userPwd;
        }
        public void setUserPwd(String userPwd) {
            this.userPwd = userPwd;
        }

    }

}
