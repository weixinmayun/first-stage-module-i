package work02;

import java.io.File;

public class Deletefile {
    public static void delete(File fileDir) {
       File[] filesArray=fileDir.listFiles();
       for(File item:filesArray){
           if(item.isFile()){
               item.delete();
               System.out.println("删除"+item.getAbsolutePath()+"成功!");
           }
           else {
               forListDirectory(item);
               item.delete();
           }
       }
    }
    // 如果某层是目录递归 删除
    public  static  void forListDirectory(File childFileDir){
        File[] childFilesArray=childFileDir.listFiles();
        for(File childItem:childFilesArray){
            if(childItem.isDirectory()){
                forListDirectory(childItem);
            }
            else {
                System.out.println("删除"+childItem+"成功!");
                childItem.delete();
            }
        }

    }
}
