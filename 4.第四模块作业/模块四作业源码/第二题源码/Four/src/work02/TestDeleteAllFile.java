package work02;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TestDeleteAllFile {
    public static void main(String[] args) throws IOException, InterruptedException {
        //指定文件夹下的文件
        String Dir = "E:\\Testtwo";
        File fileDir = new File(Dir);
        //如果不存在文件夹就新建一个文件夹 然后在文件夹下创建一个文件
        if (!fileDir.exists()) {
            System.out.println("指定的文件夹，不存在,请选择存在的文件夹");
        }
         else {
             Deletefile.delete(fileDir);
        }
    }
}
