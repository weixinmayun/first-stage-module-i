package work01;

import work01.Student;

import java.util.List;

public class SearchSingle {
    //得到单个学生
    public static Student searchSingleStudent(int studentId, List<Student> listStudent) {
        for (Student student : listStudent) {
            if (student.getStudentId() == studentId) {
                return student;
            }
        }
        return null;
    }
}
