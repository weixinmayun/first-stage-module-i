package work01;

import java.io.*;
import java.util.List;

public   class ReadStuInfo {
    public  static List<Student>  readStuList(File filDir, ObjectInputStream oIStream,List<Student> listStudent){
        try {
            oIStream=new ObjectInputStream (new FileInputStream(filDir));
             //读取文件 从字符流转换为 对象
             Object  obj=oIStream.readObject();
             if(obj!=null) {
                 return (List<Student>) obj;
             }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            if(oIStream!=null){
                try {
                    oIStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
         return listStudent;
    }
}
