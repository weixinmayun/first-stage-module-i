package work01;

public class StudentIdException extends Exception {

    private static final long serialVersionUID = -8420046496895112562L;

    public StudentIdException(){

    }
    public StudentIdException(String studentId) {
        super(studentId);
    }
}
