package work01;

public class AgeException extends Exception {


    private static final long serialVersionUID = -1240494621834827788L;

    public AgeException(){

    }
    public AgeException(String age) {
        super(age);
    }
}
