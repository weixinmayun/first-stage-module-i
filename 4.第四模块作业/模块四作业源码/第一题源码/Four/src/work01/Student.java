package work01;

import java.io.Serializable;
import java.util.Objects;
public class Student implements Serializable {
    private static final long serialVersionUID = 3839577966367029817L;
    private  String name;//学生姓名
    private  int  studentId;//学号
    private  int age;//年龄
    public Student(String name,int studentId,int age) throws StudentIdException, AgeException {
        setName(name);
        setStudentId(studentId);
        setAge(age);
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return studentId == student.studentId &&
                age == student.age &&
                Objects.equals(name, student.name);
    }
    @Override
    public int hashCode() {
        return Objects.hash(name, studentId, age);
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getStudentId() {
        return studentId;
    }
    public void setStudentId(int studentId) throws StudentIdException {
        if(studentId<0) {
            throw  new StudentIdException("学号不能小于0");
        }
        else {
            this.studentId = studentId;
        }
    }
    public int getAge() {
        return age;
    }
    public void setAge(int age) throws AgeException {
        if(age<0) {
            throw new AgeException("年龄不能小于0岁");
        }
        else {
            this.age = age;
        }
    }
    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", studentId=" + studentId +
                ", age=" + age +
                '}';
    }
}
