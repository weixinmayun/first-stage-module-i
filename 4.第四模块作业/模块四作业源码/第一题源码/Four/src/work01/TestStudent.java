package work01;

import work01.Add;
import work01.Delete;
import work01.SearchAll;
import work01.SearchSingle;
import work01.Student;
import work01.Update;

import java.io.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
public class TestStudent {
    public static void main(String[] args) {
        List<Student> listStudent=null;
        ObjectOutputStream  oOStream=null;
        ObjectInputStream oIStream=null;
        String filePath="C:\\ListStudentInfo.txt";
        //把读写的文件内容放在该文件下   首次不存在可由代码创建
        File fileDir = new File(filePath);;
        //判断学生文件是否存在
        if(!fileDir.exists()){
             try {
                 fileDir.createNewFile();
             } catch (IOException e) {
                 e.printStackTrace();
             }
            listStudent=new LinkedList<>();
         }
        //存在说明以前存入过信息
        else {
            listStudent=ReadStuInfo.readStuList(fileDir,oIStream,listStudent);//从文件读取学生信息
            if(listStudent!=null) {
                SearchAll.allStudent(listStudent);//控制台 展示所有学生
            }
        }
        Scanner sc=new Scanner(System.in);//提示用户输入要操作的类型
        String name="";//姓名默认赋值为空
        int studentId=0;//学号默认赋值为0
        int age=0;//年龄赋值为0
        while (true){
            //请输入要操作的类型
            System.out.println("请输入要操作的类型.0退出，1添加,2修改,3删除，4查询。");
            int flag=sc.nextInt();
            Student stu=null;
            switch (flag){
                case 0:System.out.println("退出!");break;
                case 1://添加
                    System.out.println("请输入姓名");
                    name=sc.next();
                    System.out.println("学号");
                    studentId=sc.nextInt();
                    System.out.println("请输入年龄");
                    age=sc.nextInt();
                    try {
                        stu=new Student(name,studentId,age);
                    } catch (StudentIdException e) {
                        e.printStackTrace();
                    } catch (AgeException e) {
                        e.printStackTrace();
                    }
                    listStudent.add(stu);
                    break;
                case 2://修改
                    SearchAll.allStudent(listStudent);
                    System.out.println("请输入姓名:");
                    name=sc.next();
                    System.out.println("请输入学号:");
                    studentId=sc.nextInt();
                    System.out.println("请输入年龄:");
                    age=sc.nextInt();
                    try {
                        stu=new Student(name,studentId,age);
                        Update.updateStudent(name,studentId,age,stu,listStudent);
                    } catch (AgeException e) {
                        e.printStackTrace();
                    } catch (StudentIdException e) {
                        e.printStackTrace();
                    }
                    listStudent.add(stu);
                    break;
                case 3://删除
                    SearchAll.allStudent(listStudent);
                    System.out.println("请输入学号进行删除!");
                    studentId=sc.nextInt();
                    try {
                        stu=new Student(null,studentId,0);
                    } catch (StudentIdException e) {
                        e.printStackTrace();
                    } catch (AgeException e) {
                        e.printStackTrace();
                    }
                    Delete.deleteStudent(studentId,listStudent);
                    break;
                case 4://查询
                    System.out.println("请输入要查询的学号");
                    studentId=sc.nextInt();
                    stu=SearchSingle.searchSingleStudent(studentId,listStudent);
                    System.out.println("姓名:"+stu.getName()+"学号:"+stu.getStudentId()+"年龄:"+stu.getAge());
                    break;
            }
            if(flag==0){
                break;
            }
        }
        WriteStuInfo.writeStuList(oOStream,listStudent,filePath);
        sc.close();
    }
}
