package work01;

import work01.Student;

import java.util.List;

public class Update {
    //修改学生方法
    public static  void updateStudent(String name, int studentId, int age, Student student, List<Student> studentList) throws AgeException, StudentIdException {
        for (Student stu :studentList){
            int index=studentList.indexOf(stu);
            if(studentId==stu.getStudentId()){
                studentList.remove(index);
                System.out.println("修改学号为:"+studentId+"的学生信息成功!");
            }
        }

        student.setName(name);
        student.setStudentId(studentId);
        student.setAge(age);
    }
}
