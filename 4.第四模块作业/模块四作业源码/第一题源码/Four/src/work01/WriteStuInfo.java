package work01;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.List;

public class WriteStuInfo {
    public static   void writeStuList(ObjectOutputStream oOStream,List<Student> listStudent,String filePath){
        try {
            oOStream=new ObjectOutputStream(new FileOutputStream(filePath));
            oOStream.writeObject(listStudent);
        } catch (IOException e) {
            if(oOStream!=null) {
                try {
                    oOStream.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
            e.printStackTrace();
        }

    }
}
