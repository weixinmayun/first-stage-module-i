package work;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.sql.ResultSet;
import java.util.Scanner;

public class Client1 {
    private static String functionType="";//
    private String userName = "";
    private String userPwd = "";
    DataInputStream in = null;
    DataOutputStream out = null;
    private static Scanner sc;
    private static int loginType;
    public Client1(Socket socket) {
        try {
            //初始化 定义输入输出流
            in = new DataInputStream(socket.getInputStream());
            out = new DataOutputStream(socket.getOutputStream());
            sc = new Scanner(System.in);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void main(String[] args) {
        Socket socket = null;
        Client1 client = null;
        try {
            //建立连接
            socket = new Socket("127.0.0.1", 8888);
            System.out.println(socket);
            //创建客户端对象
            client = new Client1(socket);
            System.out.println("---------欢迎使用考试系统-------");
            System.out.println("请输入登录类型: [0]管理员 [1]学生");
            loginType=sc.nextInt();
            if(loginType!=0&&loginType!=1){
                System.out.println("请按提示要求登录");
                return;
            }
            //开始通信
            Thread send = client.send();
            Thread receive = client.receive();
            send.start();
            receive.start();
            receive.join();
            send.join();
            client.closeConnect();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            client.closeConnect();
            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    //创建发送消息线程向服务端发送信息
    public Thread send() {
        Thread s = new Thread(() -> {
            while (true) {
                if (userName == "" && userPwd == "") {
                    try {
                        System.out.println("请输入登录账号");
                        userName = sc.next();
                        System.out.println("请输入密码");
                        userPwd = sc.next();
                        if(loginType==0) {
                            String strJson = "{functionType:'uType' ,'userName':'" + userName + "','userPwd':'" + userPwd + "','loginType':'0' }";
                            out.writeUTF(strJson);
                        }
                        else {
                            String strJson = "{functionType:'uType' ,'userName':'" + userName + "','userPwd':'" + userPwd + "','loginType':'1' }";
                            out.writeUTF(strJson);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("-----------------------------------------------");
                    System.out.println("请输入要操作的功能类型:");
                    //管理员登录操作的是LoginType=0
                    if(loginType==0) {
                        System.out.println("\t\t 管理员系统");
                        System.out.println("[uType1]增加学生  [uType2]删除学生 [uType3]修改学生 [uType4]查询学生");
                        System.out.println("[scoreType1]添加分数 [scoreType2]删除分数 [scoreType3]修改分数 [scoreType4]查询分数分数");
                        System.out.println("[quesContentType1]添加试题 [quesContentType2] 删除[quesContentType3]修改[quesContentType4]查询试题[quesContentType5]");
                        System.out.println("[exit]退出系统");
                    }
                    //学生登录操作的是LoginType=1
                    else {
                        System.out.println("\t\t 学员系统");
                        System.out.println("[scoreType4]查询分数");
                        System.out.println("[quesContentType5]考卷试卷题 ");
                        System.out.println("[uType3]修改密码或名字");
                        System.out.println("[exit]退出系统");
                    }
                    System.out.println("-----------------------------------------------");
                    Scanner sc1 = new Scanner(System.in);
                    functionType = sc1.next();
                    if(functionType.equals("exit")){
                        System.out.println("退出系统，想继续操作，重新登录!");
                        break;
                    }
                    //学生信息操作--------------------------------------------------------
                    if (functionType.equals("uType1")) {
                        System.out.println("请输入要添加的用户名");
                        String userName=sc.next();
                        System.out.println("请输入要添加的密码");
                        String userPwd=sc.next();
                        System.out.println("请输入要添加的学号");
                        String studentId=sc.next();
                        String strJsonuType = "{functionType:'uType1' ,'userName':'"+userName+"','userPwd':'"+userPwd+"','studentId':'"+studentId+"'}";
                        try {
                            out.writeUTF(strJsonuType);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    else if (functionType.equals("uType2")) {
                        System.out.println("请输入存在的学号进行删除");
                        String studentId=sc.next();
                        String strJsonuType = "{functionType:'uType2' ,'userName':'"+userName+"','userPwd':'"+userPwd+"','studentId':'"+studentId+"'}";
                        try {
                            out.writeUTF(strJsonuType);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    else if (functionType.equals("uType3")) {
                        System.out.println("请输入存在的学号进行查询");
                        String studentId=sc.next();
                        System.out.println("请输入要修改的用户名");
                        String userName=sc.next();
                        System.out.println("请输入要修改的密码");
                        String userPwd=sc.next();
                        String strJsonuType = "{functionType:'uType3' ,'userName':'"+userName+"','userPwd':'"+userPwd+"','studentId':'"+studentId+"'}";
                        try {
                            out.writeUTF(strJsonuType);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    else if (functionType.equals("uType4")) {
                        //查询成绩
                        System.out.println("请输入学号，进行人员查询!");
                        String  studentId=sc.next();
                        String strJsonuType = "{functionType:'uType4' ,'userName':'"+userName+"','userPwd':'"+userPwd+"','studentId':'"+studentId+"'}";
                        try {
                            out.writeUTF(strJsonuType);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    //成绩操作------------------------------------------------------------
                    else if (functionType.equals("scoreType1")) {
                        System.out.println("请输入要添加的学号");
                        String studentId=sc.next();
                        System.out.println("请输入分数");
                        String score=sc.next();
                        String strJsonScore = "{functionType:'scoreType1' ,'studentId':'"+studentId+"','score':'"+score+"'}";
                        try {
                            out.writeUTF(strJsonScore);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    else if (functionType.equals("scoreType2")) {
                        System.out.println("请输入要添加的学号");
                        String studentId=sc.next();
                        String strJsonScore = "{functionType:'scoreType2' ,'studentId':'"+studentId+"','score':''}";
                        try {
                            out.writeUTF(strJsonScore);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    else if (functionType.equals("scoreType3")) {
                        System.out.println("请输入要修改的学号");
                        String studentId=sc.next();
                        System.out.println("请输入要修改的分数");
                        String score=sc.next();
                        String strJsonScore = "{functionType:'scoreType3' ,'studentId':'"+studentId+"','score':'"+score+"'}";
                        try {
                            out.writeUTF(strJsonScore);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    else if (functionType.equals("scoreType4")) {
                        //查询成绩
                        System.out.println("请输入学号，进行查询成绩!");
                        String  studentId=sc.next();
                        String strJsonScore = "{functionType:'scoreType4' ,'studentId':'"+studentId+"','score':''}";
                        try {
                            out.writeUTF(strJsonScore);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    //试题----------------------------------------------------------------
                    else if (functionType.equals("quesContentType1")) {
                        System.out.println("请输入要添加的试题信息");
                        String quesContent=sc.next();
                        System.out.println("请输入试题Id");
                        String quesId=sc.next();
                        String strJsonquesContentType = "{functionType:'quesContentType1' ,'quesContent':'"+quesContent+"','quesId':'"+quesId+"'}";
                        try {
                            out.writeUTF(strJsonquesContentType);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    else if (functionType.equals("quesContentType2")) {
                        System.out.println("请输入要删除的试题的Id");
                        String quesId=sc.next();
                        String strJsonquesContentType = "{functionType:'quesContentType2' ,'quesContent':'','quesId':'"+quesId+"'}";
                        try {
                            out.writeUTF(strJsonquesContentType);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    else if (functionType.equals("quesContentType3")) {
                        System.out.println("请输入要修改的试题Id");
                        String quesId=sc.next();
                        System.out.println("请输入要修改的试题信息");
                        String quesContent=sc.next();
                        String strJsonquesContentType = "{functionType:'quesContentType3' ,'quesContent':'"+quesContent+"','quesId':'"+quesId+"'}";
                        try {
                            out.writeUTF(strJsonquesContentType);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    else if (functionType.equals("quesContentType4")) {
                        System.out.println("请输入要查询的试题Id");
                        String quesId=sc.next();
                        String strJsonquesContentType = "{functionType:'quesContentType4' ,'quesContent':'','quesId':'"+quesId+"'}";
                        try {
                            out.writeUTF(strJsonquesContentType);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    //考试抽到的题------------------------------------------------------
                    else if (functionType.equals("quesContentType5")) {
                        System.out.println("请等待抽题，马上开始考试.......");
                        String strJsonquesContentType = "{functionType:'quesContentType5' ,'quesContent':'','quesId':''}";
                        try {
                            out.writeUTF(strJsonquesContentType);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    else {
                        System.out.println("请按提示，输入正确的操作类型!");
                    }
                }
            }
        });
        return s;
    }
    //创建接收消息线程接收服务端发来的信息
    public Thread receive() {
        Thread r = new Thread(() -> {
            while (true) {
                try {
                    String strJson = in.readUTF();
                    JSONObject jsonObject =null;
                    if(strJson!=""&&strJson!=null) {
                    jsonObject=new JSONObject(strJson);
                    }
                    if (jsonObject.get("functionType").toString().equals("uType")) {
                        System.out.println("登录服务器成功!success!"+jsonObject);
                    }
                    //添加学生成功返回的状态
                    else if (jsonObject.get("functionType").toString().equals("uType1")) {
                        System.out.println("添加学生信息成功"+jsonObject);
                    }
                    else if (jsonObject.get("functionType").toString().equals("uType3")) {
                        System.out.println("修改信息成功"+jsonObject);
                    }
                    //查询学生信息
                    else if (jsonObject.get("functionType").toString().equals("uType4")) {
                        System.out.println("查询到的学生信息为:"+jsonObject);
                    }
                    //添加分数成功返回的状态
                    else if (jsonObject.get("functionType").toString().equals("scoreType1")) {
                        System.out.println("添加分数成功"+jsonObject);
                    }
                    //查询分数信息
                    else if (jsonObject.get("functionType").toString().equals("scoreType3")) {
                        System.out.println("修改分数成功:"+jsonObject);
                    }
                    //查询分数信息
                    else if (jsonObject.get("functionType").toString().equals("scoreType4")) {
                        System.out.println("该生的成绩信息为:"+jsonObject);
                    }
                    else if (jsonObject.get("functionType").toString().equals("quesContentType1")) {
                        System.out.println("添加试题成功"+jsonObject);
                    }
                    else if (jsonObject.get("functionType").toString().equals("quesContentType1")) {
                        System.out.println("修改试题成功"+jsonObject);
                    }
                    //查询到单道试题信息
                    else if (jsonObject.get("functionType").toString().equals("quesContentType4")) {
                        System.out.println("查询到的试题信息为:"+jsonObject);
                    }
                    //考试试卷考题
                    else if (jsonObject.get("functionType").toString().equals("quesContentType5")) {
                        System.out.println("考试试题如下:"+jsonObject);
                    }
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        return r;
    }
    //关闭相关连接
    public void closeConnect() {
        if (in != null) {
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (out != null) {
            try {
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (sc != null) {
            sc.close();
        }
    }

}
