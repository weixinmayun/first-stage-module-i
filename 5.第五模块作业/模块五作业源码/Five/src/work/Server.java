package work;
import org.json.JSONException;
import org.json.JSONObject;
import work.ServerDao;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class Server {
    private ServerSocket ssk=null;//监听Socket
    private Socket sk=null;//连接Socket
    private ServerDao serverDao=null;
    //构造函数初始化
    public Server(int port) {
        try {
            ssk = new ServerSocket(port);//初始化监听端口
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    //服务端main方法 主程序
    public static void main(String[] args) {
        //传入端口初始化  并且构造方法内初始化监听Socket
        Server server = new Server(8888);
        server.startWork();
    }
    //监听连接客户端 和连接客户端
    public void startWork() {
        sk = new Socket();
        while (true) {
            try {
                System.out.println("等待客户端连接......");
                // 当没有客户端连接时，则服务器阻塞在accept方法的调用这里
                sk = ssk.accept();
                //将连接的客户端Socket记录到集合中
                System.out.println("客户端" + sk.getInetAddress() + "连接成功！");
                // 每当有一个客户端连接成功，则需要启动一个新的线程为之服务
                serverThread(sk);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
            }
        }
    }
    //因有可能多个客户端 定义线程方法  接收到聊天内容和文件 转发到客户端
    public void serverThread(Socket sk) {
        new Thread(() -> {
            // 定义输入输出流流
            DataInputStream in = null;
            DataOutputStream out = null;
            try {
                in = new DataInputStream(sk.getInputStream());
                out = new DataOutputStream(sk.getOutputStream());
                while (true) {
                    try {
                        String strJson = in.readUTF();
                        JSONObject jsonObject = new JSONObject(strJson);
                        //登录到服务端 服务端返回给客户端消息
                        if(jsonObject.get("functionType").toString().equals("uType")) {
                            String userName = jsonObject.get("userName").toString();
                            String userPwd = jsonObject.get("userPwd").toString();
                            String sql = "select * from tb_userinfo where userName='" + userName + "' and userPwd='" + userPwd + "';";
                            serverDao = new ServerDao();
                            ResultSet res = serverDao.search(sql);
                            while (res.next()) {
                                if (res.getString("userName").equals(userName) && res.getString("userPwd").equals(userPwd)) {

                                    String strJsons = "{functionType:'uType' ,'userName':'"+userName+"','userPwd':'"+userPwd+"','rturnType':'登录成功!' }";
                                    // out.writeUTF(userName + "登录服务器成功!");
                                    System.out.println(strJsons);
                                    out.writeUTF(strJsons);
                                    break;
                                }
                            }
                        }
                        //分数的增删改查
                        else if(jsonObject.get("functionType").toString().equals("scoreType1")){
                            String studentId= jsonObject.get("studentId").toString();
                            int score=Integer.parseInt(jsonObject.get("score").toString());
                            String sql="insert tb_score(studentId,score) values('"+studentId+"','"+score+"');";
                            serverDao = new ServerDao();
                            serverDao.insert(sql);
                            String strJsonScore = "{functionType:'scoreType1' ,'studentId':'" + studentId+ "','score':'"+score+"'}";
                            out.writeUTF(strJsonScore);
                        }
                        else if(jsonObject.get("functionType").toString().equals("scoreType2")){
                            String studentId= jsonObject.get("studentId").toString();
                            int score=Integer.parseInt(jsonObject.get("score").toString());
                            String sql="delete  from  tb_score    where studentId='"+studentId+"'; ";
                            serverDao = new ServerDao();
                            serverDao.delete(sql);
                        }
                        else if(jsonObject.get("functionType").toString().equals("scoreType3")){
                            String studentId= jsonObject.get("studentId").toString();
                            int score=Integer.parseInt(jsonObject.get("score").toString());
                            String sql="update tb_score  set score='"+score+"' where studentId='"+studentId+"'; ";
                            serverDao = new ServerDao();
                            serverDao.update(sql);
                            String strJsonScore = "{functionType:'scoreType3' ,'studentId':'" + studentId+ "','score':'"+score+"'}";
                            out.writeUTF(strJsonScore);
                        }
                        else if(jsonObject.get("functionType").toString().equals("scoreType4")){
                            String studentId= jsonObject.get("studentId").toString();
                            String sql="select * from tb_score where studentId='"+studentId+"';";
                            serverDao = new ServerDao();
                            ResultSet res = serverDao.search(sql);
                            while (res.next()) {
                                String strJsonScore = "{functionType:'scoreType4' ,'studentId':'" + studentId+ "','score':'"+res.getString("score")+"'}";
                                System.out.println(strJsonScore);
                                out.writeUTF(strJsonScore);
                            }
                        }
                        //试题增删改查
                        else if(jsonObject.get("functionType").toString().equals("quesContentType1")){
                            SimpleDateFormat sd=new SimpleDateFormat("yyyy:MM:dd hh:mm:ss");
                            String quesContent=jsonObject.get("quesContent").toString();
                            String quesId=jsonObject.getString("quesId").toString();
                            String sqlAdd="insert into tb_quesinfo(quesContent,storeDate,quesId) values('"+quesContent+"','"+sd.format(new Date())+"','"+quesId+"');";
                            serverDao.insert(sqlAdd);
                            String strJsonScore = "{functionType:'quesContentType1' ,'quesId':'" + quesId+ "','quesContent':'"+quesContent+"'}";
                            out.writeUTF(strJsonScore);
                        }
                        else if(jsonObject.get("functionType").toString().equals("quesContentType2")){
                            String quesId=jsonObject.get("quesId").toString();
                            String sqlDelete="delete from tb_quesinfo where quesId='"+quesId+"';";
                            serverDao.delete(sqlDelete);
                        }
                        else if(jsonObject.get("functionType").toString().equals("quesContentType3")){
                            String quesId=jsonObject.get("quesId").toString();
                            String quesContent=jsonObject.get("quesId").toString();
                            String sqlUpdate="update tb_quesinfo set quesContent='"+quesContent+"' where quesId='"+quesId+"';";
                            serverDao.update(sqlUpdate);
                            String strJsonScore = "{functionType:'quesContentType3' ,'quesId':'" + quesId+ "','quesContent':'"+quesContent+"'}";
                            out.writeUTF(strJsonScore);
                        }
                        else if(jsonObject.get("functionType").toString().equals("quesContentType4")){
                            String quesId= jsonObject.get("quesId").toString();
                            String sql="select * from tb_quesinfo   where quesId='"+quesId+"';";
                            serverDao = new ServerDao();
                            ResultSet res = serverDao.search(sql);
                            while (res.next()) {
                                String strJsonScore = "{functionType:'scoreType4' ,'quesId':'" + quesId+ "','quesContent':'"+res.getString("quesContent")+"'}";
                                System.out.println(strJsonScore);
                                out.writeUTF(strJsonScore);
                            }
                        }
                        else if(jsonObject.get("functionType").toString().equals("quesContentType5")){
                            String sql="select * from tb_quesinfo  limit 5;";
                            serverDao = new ServerDao();
                            String strQuesContent="";
                            String strQuesId="";
                            ResultSet res = serverDao.search(sql);
                            while (res.next()) {
                                strQuesContent+=res.getString("quesContent")+"|";
                                strQuesId+=res.getString("quesId")+"|";
                            }
                            String strJsonScore = "{functionType:'quesContentType5' ,quesContent:'"+strQuesContent+"',quesId:'"+strQuesId+"'}";
                            System.out.println(strJsonScore);
                            out.writeUTF(strJsonScore);
                        }
                        //学生的增删改查
                        else if(jsonObject.get("functionType").toString().equals("uType1")){
                            String userName=jsonObject.get("userName").toString();
                            String userPwd=jsonObject.get("userPwd").toString();
                            String studentId=jsonObject.get("studentId").toString();
                            String sqlAdd="insert into tb_userinfo(userName,userPwd,userType,studentIdOrTeacherId) values('"+userName+"','"+userPwd+"','1','"+studentId+"');";
                            serverDao.insert(sqlAdd);
                            String strJsonScore = "{functionType:'uType1' ,'studentId':'" + studentId+ "','userName':'"+userName+"','userPwd':'"+userPwd+"'}";
                            out.writeUTF(strJsonScore);
                        }
                        else if(jsonObject.get("functionType").toString().equals("uType2")){
                            String studentId=jsonObject.get("studentId").toString();
                            String sqlDelete="delete from tb_userinfo  where studentIdOrTeacherId='"+studentId+"'";
                            serverDao.delete(sqlDelete);
                        }
                        else if(jsonObject.get("functionType").toString().equals("uType3")){
                            String userName=jsonObject.get("userName").toString();
                            String userPwd=jsonObject.get("userPwd").toString();
                            String studentId=jsonObject.get("studentId").toString();
                            String sqlUpdate="update tb_userinfo set userName='"+userName+"',userPwd='"+userPwd+"' where studentIdOrTeacherId='"+studentId+"' ";
                            serverDao.update(sqlUpdate);
                            String strJsonScore = "{functionType:'uType3' ,'studentId':'" + studentId+ "','userName':'"+userName+"','userPwd':'"+userPwd+"'}";
                            out.writeUTF(strJsonScore);
                        }
                        else if(jsonObject.get("functionType").toString().equals("uType4")){
                            String studentId= jsonObject.get("studentId").toString();
                            String sql="select * from tb_userinfo   where  studentIdOrTeacherId='"+studentId+"';";
                            serverDao = new ServerDao();
                            ResultSet res = serverDao.search(sql);
                            while (res.next()) {
                                String strJsonScore = "{functionType:'uType4' ,'studentId':'" + studentId+ "','userName':'"+res.getString("userName")+"','userPwd':'"+res.getString("userPwd")+"'}";
                                System.out.println(strJsonScore);
                                out.writeUTF(strJsonScore);
                            }
                        }
                    } catch (IOException | JSONException | SQLException e) {
                        e.printStackTrace();
                    } finally {
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }).start();
    }
}
