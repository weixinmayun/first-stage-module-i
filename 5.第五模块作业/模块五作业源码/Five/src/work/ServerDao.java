package work;
import work.DBHelper;

import java.sql.ResultSet;
import java.sql.SQLException;
public class ServerDao {
    public  ServerDao(){}
    //添加
    public void insert(String sql) {
        DBHelper dbHelper = new DBHelper();
        int count = dbHelper.executeOperate(sql);
        if(count>0) {
            System.out.println("添加成功!");
        }
        else {
            System.out.println("添加失败！");
        }
    }
    //修改
    public void update(String sql) {
        DBHelper dbHelper = new DBHelper();
        int count = dbHelper.executeOperate(sql);
        if(count>0) {
            System.out.println("修改成功!");
        }
        else {
            System.out.println("修改失败！");
        }
    }
    //删除
    public void delete(String sql){
        DBHelper dbHelper = new DBHelper();
        int count = dbHelper.executeOperate(sql);
        if(count>0) {
            System.out.println("删除成功!");
        }
        else {
            System.out.println("删除失败！");
        }
    }
    //查询
    public ResultSet search(String sql){
        ResultSet res = null;
        DBHelper dbHelper = new DBHelper();
        res = dbHelper.executeQuery(sql);
        return res;
    }

}
