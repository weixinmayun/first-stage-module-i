package firstquestion;

import java.util.Random;

public class Diagonal {
    private  int[][] arr;
    public  Diagonal(int rows,int  cols) {
        arr= new int[rows][cols];
    }
    //打印二维数组，并为二维数组赋值
    public void  printingArr(){
        Random rd=new Random();
        for (int i=0;i<arr.length;i++) {
            for(int j=0;j<arr[0].length;j++){
                arr[i][j]=rd.nextInt(10);
                System.out.print(arr[i][j]+" ");
            }
            System.out.println();
        }
    }
    //计算每行的总和累加=行总和
    public  void getRowsSum(){
        int  rowsum=0;
        for(int i=0;i<arr.length;i++) {
            int everyRowSum=0;
            for(int j=0;j<arr[0].length;j++){
                everyRowSum+=arr[i][j];
                rowsum+=arr[i][j];
            }
            System.out.println("第"+(i+1)+"行总和为:"+everyRowSum);
        }
        System.out.println("所有行总和为:"+rowsum);
        System.out.println("--------------------------------------");

    }
    //计算每列的总和累加=列总和
    public void getColsSum(){
        int rowSum=0;
        for(int i=0;i<arr[0].length;i++) { //列数遍历
            int everyRowSum=0;
            for(int j=0;j<arr.length;j++){ //行数遍历
                everyRowSum+=arr[j][i];
                rowSum+=arr[j][i];
            }
            System.out.println("第"+(i+1)+"列总和为:"+everyRowSum);
        }
        System.out.println("所有列总和为:"+rowSum);
        System.out.println("--------------------------------------");
    };
    //计算左上到右下 对脚线的和
    public void   getFirstSum(){
        int FirstSum=0;
        for(int i=0;i<arr.length;i++) {
            FirstSum+=arr[i][i];
        }
        System.out.println("左上到右下，对角线的和为:"+FirstSum);
        System.out.println("--------------------------------------");
    }
    //右上到左下 对脚线的和
    public void getSecondSum(){
        int secondeSum=0;
        for(int i=0;i<arr.length;i++){
            secondeSum+=arr[arr[0].length-1-i][i];
        }
        System.out.println("右上到左下对角线的总和为:"+secondeSum);
    }

}