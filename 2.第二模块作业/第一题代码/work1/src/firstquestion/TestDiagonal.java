package firstquestion;
import java.util.Arrays;
import java.util.Scanner;

public class TestDiagonal {
    public static void main(String[] args) {

        Diagonal dg=new Diagonal(16,16);
        dg.printingArr();// 为数组赋值随机数，并打印数组
        dg.getRowsSum(); //得到每行的和
        dg.getColsSum();//得到每列的和
        dg.getFirstSum();//得到从左上到右下对角线的和
        dg.getSecondSum();//得到从右上到左下对角线的和
    }
}
