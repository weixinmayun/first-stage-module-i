package threequestion;

public class TestConsumeCase {
    public static void main(String[] args) {

        //定义通话套餐 资费 用量
        PackageCorrespond  correspond=new PackageCorrespond(1,500);
        //定义上网套餐  资费 用量
        PackageNetwork network=new PackageNetwork(1,50);
        //创造消费统计对象　　
        Consume consume=new Consume();
        //注册一张手机卡,枚举手机卡类型:大卡
        MobileCard  card=new MobileCard(CardEnum.SMALL.getCardType(),"15495648994","admin","123456",1000,500,100,consume);
        System.out.println("注册手机卡......");
        //这句 方法内部实现了多态  抽象类的引用指向了两个子类 通信类 上网套餐内
        card.showUserMessage(correspond,network); //把通信套餐 上网套餐注册 给 手机卡
        System.out.println("-----------------------");
        System.out.println("通话消费......");
        correspond.service(50,card);
        System.out.println("-----------------------");
        System.out.println("流量消费......");
        network.service(10,card);

    }
}
