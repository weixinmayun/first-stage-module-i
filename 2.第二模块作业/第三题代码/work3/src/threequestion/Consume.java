package threequestion;


import java.math.BigDecimal;

public class Consume {
    private  int  callDuration;//通话时长
    private  int  netWorkFlow;//上网流量
    private int  consumePrices;//统计消费金额

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    private  int balance;
    public int getCallDuration() {
        return callDuration;
    }
    public void setCallDuration(int callDuration) {
        this.callDuration = callDuration;
    }
    public int getNetWorkFlow() {
        return netWorkFlow;
    }
    public void setNetWorkFlow(int netWorkFlow) {
        this.netWorkFlow = netWorkFlow;
    }
    public int getConsumePrices() {
        return consumePrices;
    }
    public void setConsumePrices(int consumePrices) {
        this.consumePrices = consumePrices;
    }
    public  void  showConsumeMessage(){
        System.out.println("消费情况:已通话"+getCallDuration()+"分钟;已使用流量"+getNetWorkFlow()+"GB;累计消费金额"+getConsumePrices()+"元;还剩余额"+getBalance()+"元");

    }

}
