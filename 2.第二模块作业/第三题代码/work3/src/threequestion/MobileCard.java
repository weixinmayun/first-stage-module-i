package threequestion;

import java.math.BigDecimal;

public class MobileCard {
    public   PackageCorrespond  correspond; //通信信息类
    public   PackageNetwork  network; //流量信息类
    private  String cardType;//卡类型，枚举
    private  String cardNumber;//电话号码
    private  String userName;//用户名
    private  String userPwd;//密码
    private  int accountBalance;//余额
    private  int callDuration;//通话套餐通话时长(分钟)
    private  int netWorkFlow;//上网套餐流量
    private  Consume userConsume; //消费统计对象
    public Consume getUserConsume() {
        return userConsume;
    }
    public void setUserConsume(Consume userConsume) {
        this.userConsume = userConsume;
    }
    public String getCardType() {
        return cardType;
    }
    public void setCardType(String cardType) {
        this.cardType = cardType;
    }
    public String getCardNumber() {
        return cardNumber;
    }
    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }
    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }
    public String getUserPwd() {
        return userPwd;
    }
    public void setUserPwd(String userPwd) {
        this.userPwd = userPwd;
    }
    public int getAccountBalance() {
        return accountBalance;
    }
    public void setAccountBalance(int accountBalance) {
        this.accountBalance = accountBalance;
    }
    public int getCallDuration() {
        return callDuration;
    }
    public void setCallDuration(int callDuration) {
        this.callDuration = callDuration;
    }
    public int getNetWorkFlow() {
        return netWorkFlow;
    }
    public void setNetWorkFlow(int netWorkFlow) {
        this.netWorkFlow = netWorkFlow;
    }
    public MobileCard(String cardType,String cardNumber,String userName,String userPwd,int accountBalance,int callDuration,int netWorkFlow,Consume userConsume){
        setCardType(cardType);
        setCardNumber(cardNumber);
        setUserName(userName);
        setUserPwd(userPwd);
        setAccountBalance(accountBalance);
        setCallDuration(callDuration);
        setNetWorkFlow(netWorkFlow);
        setUserConsume(userConsume);
    }
    public  void   showCardMessage(){
        System.out.println("卡号:"+cardNumber+"用户名:"+userName+"当前余额:"+accountBalance);
    }
    //填加消费
    public void addConsume(int consumePrice,int consumeDuration,int consumeNetWorkFlow){
        getUserConsume().setBalance(getAccountBalance());
        getUserConsume().setConsumePrices(consumePrice+getUserConsume().getConsumePrices());
        getUserConsume().setCallDuration(consumeDuration);
        getUserConsume().setNetWorkFlow(getUserConsume().getNetWorkFlow()+consumeNetWorkFlow);
        getUserConsume().showConsumeMessage();
    }
    //注册用户基本信息
    public  void  showUserMessage(PackageCorrespond  correspond,PackageNetwork  network)
    {
        System.out.printf("用户名:%s\n密码:%s\n卡号:%s\n卡类型:%s\n",getUserName(),getUserPwd(),getCardNumber(),getCardType(),getCallDuration(),getNetWorkFlow(),getAccountBalance());
        AbstractPackageMeal c=correspond; //多态的体现
        AbstractPackageMeal n=network;//多态的体现
        if(c!=null) {//判断对象是否为空 空 对象.方法报错
            c.showPackageMeal();
        }
        if(n!=null) {
            n.showPackageMeal();
        }
    }
}
