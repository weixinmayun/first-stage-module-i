package threequestion;


public enum CardEnum {
    BIG("大卡"), SMALL("小卡"), MINI("微型卡");
    private  final String cardType;
    private CardEnum(String carType) {
        this.cardType=carType;
    }
    public  String getCardType(){
        return  cardType;
    }
}