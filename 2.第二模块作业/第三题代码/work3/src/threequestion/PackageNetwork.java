package threequestion;

import java.math.BigDecimal;
public class PackageNetwork extends  AbstractPackageMeal implements IPackageNetwork {
    public   PackageCorrespond  correspond;
    public   PackageNetwork  network;
    public PackageNetwork(int price, int packageMealCount) {
        super(price, packageMealCount);
    }
    //重写公共抽象类的方法
    @Override
    public void showPackageMeal() {
        System.out.println("上网套餐:流量包"+getPackageMealCount()+"GB;每月资费"+getPrice()+"元/GB");
    }
    //重写接口中上网套餐接口的方法
    @Override
    public void service(int netWorkFlow, MobileCard mobileCard) {
       //消费金额
        int consumePrices=netWorkFlow*getPrice();
        //余额-消费金额=实际余额
       mobileCard.setAccountBalance(mobileCard.getAccountBalance()-consumePrices);
        mobileCard.showUserMessage(correspond,network);
        //添加消费
       mobileCard.addConsume(consumePrices,mobileCard.getCallDuration(),netWorkFlow);

    }
}
