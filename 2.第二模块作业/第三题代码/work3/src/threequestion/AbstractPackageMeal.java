package threequestion;

import java.math.BigDecimal;

public abstract class AbstractPackageMeal {
    private int price; //套餐资费
    private  int packageMealCount; //套餐数量
    public int getPrice() {
        return price;
    }//得到套餐资费
    //设置套餐资费
    public void setPrice(int price) {
        this.price = price;
    }
    //得到套餐数量
    public int getPackageMealCount() {
        return packageMealCount;
    }
    //设置套餐数量
    public void setPackageMealCount(int packageMealCount) {
        this.packageMealCount = packageMealCount;
    }
    //构造函数 初始化套餐
    public AbstractPackageMeal(int price,int packageMealCount){
        setPrice(price);
        setPackageMealCount(packageMealCount);
    }
    //展示套餐信息抽象方法
    public abstract void showPackageMeal();
}
