package threequestion;

import java.math.BigDecimal;

public class PackageCorrespond extends AbstractPackageMeal implements  IPackageCorrespond {
    public   PackageCorrespond  correspond;
    public   PackageNetwork  network;
    private int  messageCount;//短信条数
    public int getMessageCount() {
        return messageCount;
    }
    public void setMessageCount(int messageCount) {
        this.messageCount = messageCount;
    }
    public PackageCorrespond(int price, int packageMealCount) {
        super(price, packageMealCount);
    }
    //重写公共抽象类的方法
    @Override
    public void showPackageMeal() {
        System.out.println("通话套餐:通话时长时包"+getPackageMealCount()+"分钟;每月资费"+getPrice()+"元/分钟;短信:"+getMessageCount()+"条");
    }
    //重写接口中通信的方法
    @Override
    public void service(int consumecallDuration, MobileCard mobilecard) {
        //消费金额
        int consumePrices=consumecallDuration*getPrice();
        //余额 -消费=剩余金额
       mobilecard.setAccountBalance(mobilecard.getAccountBalance()-consumePrices);
        mobilecard.setCallDuration(consumecallDuration);
        //打印通话消费信息
        mobilecard.showUserMessage(correspond,network);
        //添加通话时长
        mobilecard.addConsume(consumePrices,consumecallDuration,0);

    }
}
