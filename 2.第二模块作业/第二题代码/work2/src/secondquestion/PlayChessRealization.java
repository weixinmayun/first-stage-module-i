package secondquestion;

import java.util.Scanner;

public class PlayChessRealization {
    private String[][] board;
    private char black;
    private char white;
    private int rows;
    private int cols;
    public boolean isEnd = true;  //判断有时是否结束的标识  true标识正在对局，false决出胜负
    public String competitionResult;

    PlayChessRealization(int rows, int cols) { //需要设置初始值 定义一个构造方法
        setBoard(rows, cols);
    }

    //打印棋盘
    private void setBoard(int rows, int cols) {
        board = new String[rows][cols];
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {
                if (i == 0) {
                    board[i][j] = String.format("%x", j - 1);
                } else if (j == 0) {
                    board[i][j] = String.format("%x", i - 1);
                } else {
                    board[i][j] = "+";
                }
                board[0][0] = " ";
            }

        }
    }

    private void getBoard() {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {
                System.out.print(board[i][j] + "      ");
            }
            System.out.println();
        }
    }

    //开始落子对战  提示用户输入黑白子 重新加载 棋盘内容
    public void beginPalyGame() {
        int isRepeatFlag = 0; //判断输入的棋子是否超出索引界限
        getBoard();
        String playId = "黑方";//标识落子的操作者Id，默认黑。 共两种状态，（黑）表示人物一落子，（白）表示人物二落子
        String chess = "";//棋子如果是黑赋值 字符串chess=$   如果白赋值chess=@
        System.out.println("游戏开始了,默认黑子先落子，请落子!");
        Scanner sc = new Scanner(System.in);
        int x = 0;//在本方法中是全局变量，因while多次使用需要赋初值，否则提示未初始化。那定义一个不在落子范围的初始值
        int y = 0;
        //黑子先行
        try {
            //设置黑子默认先行  请输入黑子横坐标
            System.out.println("请输入黑方棋子横坐标:");
            x = sc.nextInt() + 1;
            //设置黑子默认先行  请输入黑子纵坐标
            System.out.println("请输入黑方棋子纵坐标:");
            y = sc.nextInt() + 1;
            if ((x <= 1 | x > 17) | (y <= 1 | y > 17)) { //该条件不在棋盘范围内
                // isInBoard(isRepeatFlag,x,y,sc,playId);//提醒用户直到落下的棋子在棋盘范围
                System.out.println("请看清楚，没有把棋子落到棋盘上，超出索引界限");
                isRepeatFlag = 1;
                while (isRepeatFlag == 1) {
                    x = 0;
                    y = 0;
                    System.out.println("请重新输入" + playId + "棋子横坐标:");
                    x = sc.nextInt() + 1;
                    //设置黑子默认先行  请输入黑子纵坐标
                    System.out.println("请重新输入" + playId + "棋子纵坐标:");
                    y = sc.nextInt() + 1;
                    if ((x <= 1 || x > 17) || (y <= 1 || y > 17)) {  //如果输入横纵坐标不在这个范围，提示用户输入合法格式
                        isRepeatFlag = 1;
                        System.out.println("请看清楚，没有把棋子落到棋盘上，超出索引界限！");
                        continue;
                    } else {
                        isRepeatFlag = 0;
                        break;
                    }
                }
            }
            playId = "白方";
            board[x][y] = "*";//第一步设置是黑子先
            getBoard();
        } catch (Exception ex) {
            System.out.println("请按合法的格式重新落子!");
        }

        while (isEnd == true) {
            //如果输入的不是数字，抛出异常请用户重新输入
            try {
                System.out.println("请输入" + playId + "棋子横坐标:");
                x = sc.nextInt() + 1;
                System.out.println("请输入" + playId + "棋子纵坐标:");
                y = sc.nextInt() + 1;
                if ((x <= 1 || x > 17) || (y <= 1 || y > 17)) {  //该条件不在棋盘范围内
                    // isInBoard(isRepeatFlag,x,y,sc,playId); //提醒用户直到落下的棋子在棋盘范围
                    System.out.println("请看清楚，没有把棋子落到棋盘上超出索引界限");
                    isRepeatFlag = 1;
                    while (isRepeatFlag == 1) {
                        x = 0;
                        y = 0;
                        System.out.println("请重新输入" + playId + "棋子横坐标:");
                        x = sc.nextInt() + 1;
                        //设置黑子默认先行  请输入黑子纵坐标
                        System.out.println("请重新输入" + playId + "棋子纵坐标:");
                        y = sc.nextInt() + 1;
                        if ((x <= 1 || x > 17) || (y <= 1 || y > 17)) {  //如果输入横纵坐标不在这个范围，提示用户输入合法格式
                            isRepeatFlag = 1;
                            System.out.println("请看清楚，没有把棋子落到棋盘上，超出索引界限！");
                            continue;
                        } else {
                            isRepeatFlag = 0;
                            break;
                        }
                    }
                }
            } catch (Exception ex) {
                System.out.println("请按合法的格式重新落子!");
            }
            if (board[x][y] == "+") {
                if (playId == "黑方") {
                    chess = "$";
                    playId = "白方";
                } else {
                    chess = "@";
                    playId = "黑方";
                }
                board[x][y] = chess;
                String result = competitionResult(chess, x, y);
                if (result == "黑方") {
                    System.out.println("黑子获胜!");
                    break;
                } else if (result == "白方") {
                    System.out.println("白子获胜!");
                    break;
                } else if (result == "平局") {
                    System.out.println("平局");
                    break;
                }

                getBoard();
            } else {
                System.out.println("该位置已落子，请选择空闲的位置落子！");
                continue;
            }
        }
        sc.close();
    }

    //计算胜负
    public String competitionResult(String chess, int x, int y) {
        String result = "";
        int countCol = -1;
        int countRow = -1;
        int countDiagonalLeft = -1;
        int countDiagonalRight = -1;
        int a;
        int b;
        // 行：向左数相同的棋子
        a = x;
        b = y;
        //以下都无限循环 直到遇到 终止的条件 break;
        while (true) {
            if (chess.equals(board[a][b])) {
                countRow++;
                // 防止数组下标越界
                if (b == 0) {
                    break;
                }
                b--;
            } else {
                break;
            }
        }
        // 行：向右数相同的棋子
        a = x;
        b = y;
        while (true) {
            if (chess.equals(board[a][b])) {
                countRow++;
                // 防止数组下标越界
                if (b == (cols - 1)) {
                    break;
                }
                b++;
            } else {
                break;
            }
        }
        // 列：向下数相同的棋子
        a = x;
        b = y;
        while (true) {
            if (chess.equals(board[a][b])) {
                countCol++;
                // 防止数组下标越界
                if (a == 0) {
                    break;
                }
                a--;
            } else {
                break;
            }
        }
        // 列：向上数相同的棋子
        a = x;
        b = y;
        while (true) {
            if (chess.equals(board[a][b])) {
                countCol++;
                // 防止数组下标越界
                if (a == (rows - 1)) {
                    break;
                }
                a++;
            } else {
                break;
            }
        }
        // 左对角：左上数相同的棋子
        a = x;
        b = y;
        while (true) {
            if (chess.equals(board[a][b])) {
                countDiagonalLeft++;
                // 防止数组下标越界
                if (a == 0 || b == 0) {
                    break;
                }
                a--;
                b--;
            } else {
                break;
            }
        }
        // 左对角：右下数相同的棋子
        a = x;
        b = y;
        while (true) {
            if (chess.equals(board[a][b])) {
                countDiagonalLeft++;
                // 防止数组下标越界
                if (a == (rows - 1) || b == (cols - 1)) {
                    break;
                }
                a++;
                b++;
            } else {
                break;
            }
        }

        // 右对角：右上数相同的棋子
        a = x;
        b = y;
        while (true) {
            if (chess.equals(board[a][b])) {
                countDiagonalRight++;
                // 防止数组下标越界
                if (a == 0 || b == (cols - 1)) {
                    break;
                }
                a--;
                b++;
            } else {
                break;
            }
        }
        // 右对角：左下数相同的棋子
        a = x;
        b = y;
        while (true) {
            if (chess.equals(board[a][b])) {
                countDiagonalRight++;
                // 防止数组下标越界
                if (a == (rows - 1) || b == 0) {
                    break;
                }
                a++;
                b--;
            } else {
                break;
            }
        }
        //有一方连续5个决出胜负结束
        if (countRow >= 5 || countCol >= 5 || countDiagonalLeft >= 5 || countDiagonalRight >= 5) {
            result = chess + "方";
            isEnd = false;
            System.out.println(chess + "方获胜!");
            return result;
        }
        //棋盘已占满未决出胜负
        String content = "";//存储棋盘已落子个数
        outer:
        for (int i = 1; i < board.length; i++) { //第一行不在棋盘上 不做判断 所以i=1
            for (int j = 1; j < board[0].length; j++) {//第一列不在棋盘上 不做判断 所以j=1
                if (board[i][j] == "+") {
                    break outer;
                } else {
                    content += board[i][j];
                }
            }
        }
        //16行16列是256个棋子,大于等于改数字说明棋盘已占满
        if (content.length() >= 256) {
            result = "平局";
            isEnd = false;
            System.out.println("平局");
            return result;
        }
        return result;
    }
}
