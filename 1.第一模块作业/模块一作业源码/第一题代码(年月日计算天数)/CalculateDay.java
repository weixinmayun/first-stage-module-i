import java.util.Scanner;
public class CalculateDay {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("请输入年份:");
        int year=sc.nextInt();
        System.out.println("请输入月份:");
        int month=sc.nextInt();
        System.out.println("请输入天:");
        int day=sc.nextInt();
        if(year>0) {
            //平年二月28 瑞年二月29    1,3,5,7,8,10,12（每月31年总不差）  4,6,9，11（30整）
            //瑞年  年份与4,100取余等于0  或者 年份与400取余等于0
            if((year%100==0&&year%4==0)||year%400==0) {//瑞年
                cacaulateMonth(month,"rui",day);//传入变量rui说明是瑞年的意思
            }
            else {//平年
                cacaulateMonth(month,"common",day);//传入变量为common说明是平年
            }
        }
        else {
            System.out.println("请输入正确的年份！");
        }
    }
    public  static  void  cacaulateMonth(int month,String yearState,int day) {
        int sumDay = 0;
       if (month == 1)//输入一月直接输入即可
        {
            System.out.println("该年月日为本年度的第" + day+"天");
        }
        else if (month ==2)//输入二月直接输入即可
        {
            if (yearState == "common") {
                if (day >= 1 && day <= 28) {
                    sumDay = day + 31;
                }
                else {
                    System.out.println("请输入合法的日！");
                }
            } else {
                if (day >= 1 && day <= 29){
                    sumDay = day + 31;
                }
                else {
                    System.out.println("请输入合法的日！");
                }
            }
            System.out.println("该年月日为本年度的第" + sumDay+"天");
        } else { //三月遍历累加
            if (yearState == "common") {//平年一月加二月
                sumDay = 28+31;
            } else {//瑞年一月加二月
                sumDay = 29+31;
            }
            if (month > 2 && month  <= 12) { //大于三月循环遍历累加
                sumDay += day;
                 int monthm=month - 1;// 减一的原因是因为输入天数 可能不是满一个月 ，所以在前面逻辑先计算一月二月 ,然后循环大于三月的遍历累加
                while (monthm> 2) {
                    if ( (monthm == 3 || monthm == 5 || monthm == 7 || monthm == 8 || monthm == 10 || monthm == 12)) {
                        sumDay += 31;
                    } else if (monthm == 4 || monthm == 6 || monthm == 9|| monthm == 11) {
                        sumDay += 30;
                    } else {
                        System.out.println("请输入合法的日！");
                    }
                    monthm--;
                }
            } else {
                System.out.println("请输入正取的月份！");
            }
           System.out.println("该年月日为本年度的第" + sumDay+"天");
        }
    }
}
