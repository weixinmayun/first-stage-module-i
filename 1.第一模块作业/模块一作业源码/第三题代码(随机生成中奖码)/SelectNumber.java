import java.util.Arrays;
import  java.util.Random;
public class SelectNumber {
    public static void main(String[] args) {
        Random rd = new Random();
        int[] red = new int[6];
        int num;
        int i = 0;
        outer:
        while (i < red.length) {//通过数组长度遍历
            num = rd.nextInt(33) + 1; //每次生成随机数
            for (int j = 0; j < red.length; j++) {//遍历数组中已存在的随机数，判断和新生成的随机数是否重复
                if (red[j] == num) { //新生成的和数组中已存在的重复就退出当前双城循环
                    System.out.println("该数字，已存在，重来");
                    continue outer;
                }
            }
            red[i] = num; //不是重复的就存入数组
            i++;
        }
        // 随机一个蓝号
        int blueNumber = rd.nextInt(16) + 1;

        // 打印结果
        System.out.println("红号：" + Arrays.toString(red) + "\n蓝号：" + blueNumber);
    }
}
