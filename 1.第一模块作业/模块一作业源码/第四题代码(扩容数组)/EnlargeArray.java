import  java.util.Arrays;
public class EnlargeArray {
    public static void main(String[] args) {
        int[] arr = new int[10];//定义一个长度为10的数组
        int i = 1;
        while (i < 9){
            if ((i+1) * 1.0 / arr.length >0.8) {
                System.out.println("超出数组容量80%，进行" + 1.5 + "倍扩容");

                int[] newArr = new int[(int) (arr.length * 1.5)];    // 定义一个新数组，长度为原数组1.5倍
                // 赋值原数组数据到新数组
                System.arraycopy(arr, 0, newArr, 0, arr.length);
                arr = newArr;
            }
              arr[i] = i;
            System.out.printf("添加第%d个元素，当前数组长度为%d:%s\n", i + 1, arr.length, Arrays.toString(arr));
            i += 1;
        }
    }
}
