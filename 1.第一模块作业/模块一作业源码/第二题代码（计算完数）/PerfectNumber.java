public class PerfectNumber {
    public static void main(String[] args) {
        String allNumber="";
        for(int i=1;i<=1000;i++){//遍历1000次去找该数因子，和完数
            int sum=0;
            int num=1;
            for(;num<i;num++){ //每次都去遍历找因子
                if(i%num==0) { //如果有因子就去累加
                    sum+=num;
                }
            }
            if(sum==num) { //把1000以内有完数的都累加
                allNumber+=sum+",";
            }
        }
        System.out.println("1000之内所有完数为:"+allNumber.substring(0,allNumber.length()-1));
    }
}