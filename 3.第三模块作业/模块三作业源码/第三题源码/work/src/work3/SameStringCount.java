package work3;

import java.util.HashMap;
import java.util.Map;

public class SameStringCount {
    public static void main(String[] args) {
        String str="123,456,789,123,456";
        //通过逗号分隔字符串
        String[] strArr=str.split(",");
        //HashMap集合
        Map<String,Integer> hs=new HashMap<>();
        //遍历字符串 把字符串添加到HashMap 键为字符串  值为字符串出现的次数
        for(int i=0;i<strArr.length;i++){
            if(hs.containsKey(strArr[i])){
                hs.put(strArr[i],hs.get(strArr[i])+1);
            }
            else {
                hs.put(strArr[i],1);
            }
        }
        //把HashMap存入的键值 遍历输出
        for (String key :hs.keySet()) {
            System.out.println(key+"出现次数为:"+hs.get(key));
        }
    }
}
