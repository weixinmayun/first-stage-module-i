package work2;

public class BigString {
    public static void main(String[] args) {
      String  s1="asdafghjka";//字符串1
      String  s2="aaasdfg";//字符串2
      String  maxStr="";//大串
      String  minStr="";//小串
      String  sonStr="";//字串
      String  maxSonStr="";//最大字串
        // 判断s1和s2谁是大串和小串
      if(s1.length()>s2.length()) {
          maxStr=s1;
          minStr=s2;
      }
      else{
         maxStr=s2;
         minStr=s1;
      }
       //两个串  大串是否包含小串
      if(maxStr.contains(minStr)){
             System.out.println("最小字串为:"+minStr);
             return;
      }
      //依次缩减小串长度来遍历  看大串中是否包含  如果有说明是最大字串跳出循环
      outer:for (int i =1; i <minStr.length() ; i++) {
          for (int j = 0; j <=i ; j++) {
              sonStr=minStr.substring(j,minStr.length()-i);//字串
              if(maxStr.contains(sonStr)){
                  maxSonStr=sonStr;
                  break outer;
              }
          }
      }
      System.out.println("最大字串为:"+maxSonStr);
    }
}
