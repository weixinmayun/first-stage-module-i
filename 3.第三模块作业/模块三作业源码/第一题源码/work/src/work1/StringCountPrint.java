package work1;

public class StringCountPrint {
    public static void main(String[] args) {
        String str="ABCD123!@#$%ab";//字符串
        char[] charArr=str.toCharArray();//将字符串转换为字符数组
        String toUpper="";//存大写字符串
        int toUpperCount=0;//大写字符串数量
        String toLower="";//存小写字符串
        int toLowerCount=0;//小写字符串数量
        String toDigital="";//存数字
        int toDigitalCount=0;//数字数量
        String toOther="";//存其它字符串
        int toOtherCount=0;//其它字符串数量
        for(char ch: charArr){
          //大写字符
          if(Character.isUpperCase(ch)){
              ++toUpperCount;
              toUpper+=ch;
          }
          //小写字符
          if(Character.isLowerCase(ch)){
            ++toLowerCount;
            toLower+=ch;
          }
          //数字
          if(Character.isDigit(ch)){
               ++toDigitalCount;
               toDigital+=ch;
          }
          //其它字符串
          if(!Character.isLetterOrDigit(ch)){
              ++toOtherCount;
              toOther+=ch;
          }
      }
        System.out.println("大写字母是:"+toUpper+"共"+toUpperCount+"个.");
        System.out.println("小写字母是:"+toLower+"共"+toLowerCount+"个.");
        System.out.println("数字是:"+toDigital+"共"+toDigitalCount+"个.");
        System.out.println("其它字符是:"+toOther+"共"+toOtherCount+"个.");
    }
}
