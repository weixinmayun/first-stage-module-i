package work5;
import java.util.*;
public class Card {
    //扑克牌
    private static String[] values = { "大王", "小王", "2", "A", "K", "Q", "J", "10", "9", "8", "7", "6", "5", "4", "3" };
   //扑克牌花色
    private static String[] types = { "红桃", "黑桃", "梅花", "方块" };
    private static Map<Integer, String> cards = new HashMap<>();
    private static List<Integer> order = new ArrayList<>();
    private static List<Integer> a = new ArrayList<>();//a玩家
    private static List<Integer> b = new ArrayList<>();//b玩家
    private static List<Integer> c = new ArrayList<>();//c玩家
    private static List<Integer> d = new ArrayList<>();//d底牌
    //获取扑克牌
    public  static void getCards(){
       int index=1;
        for (int i = 0; i <values.length; i++) {
            for(int j=0;j<types.length;j++){
                if(i<2){
                    cards.put(index,types[j]+values[i]);
                    order.add(index);
                    ++index;
                    break;
                }
                else {
                    cards.put(index,types[j]+values[i]);
                    order.add(index);
                    ++index;
                }
            }
        }
    }
    //洗牌
    public  static void shuffle(){
        Collections.shuffle(order);
    }
    //发牌
    public  static void distribute(){
        for(int i=0;i<order.size();i++){
            int cardId=order.get(i);
            if(i<3){
                d.add(cardId);
            }
            else  if(i%3==1){
                 a.add(cardId);
            }
            else if(i%3==2){
                b.add(cardId);
            }
            else {
                c.add(cardId);
            }
        }
        Collections.sort(a);
        Collections.sort(b);
        Collections.sort(c);
    }
    //显示牌
    public  static void showCardsInfo(){
        System.out.println("a玩家:");
        printCards(a);
        System.out.println("\nb玩家:");
        printCards(b);
        System.out.println("\nc玩家:");
        printCards(c);
        System.out.println("\n底牌：");
        printCards(d);

    }
    //打印单个消息
    private static void printCards(List<Integer> list){
        for(Integer item:list){
            System.out.print(cards.get(item));
        }
    }
}
