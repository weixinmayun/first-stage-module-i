package work4;

import java.util.List;

public class Add {
    //添加学生的方法
    public static void addStudent(String name, int studentId, int age, List<Student> liststudent) {
        Student newstudent = new Student(name, studentId, age);
        if (liststudent.contains(newstudent)) {
            System.out.println("学号为" + studentId + "已存在!");
        } else {
            liststudent.add(newstudent);
            System.out.println("学生" + name + "添加成功!");
        }
    }
}
