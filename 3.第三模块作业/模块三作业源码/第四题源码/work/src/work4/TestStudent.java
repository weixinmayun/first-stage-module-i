package work4;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
public class TestStudent {
    public static void main(String[] args) {
        List<Student> listStudent=new LinkedList<>();
        SearchAll.allStudent(listStudent);//加载所有学生，初次加载为空
        Scanner sc=new Scanner(System.in);//提示用户输入要操作的类型
        String name="";//姓名默认赋值为空
        int studentId=0;//学号默认赋值为0
        int age=0;//年龄赋值为0
        while (true){
            //请输入要操作的类型
            System.out.println("请输入要操作的类型:");//  s=0退出，1添加,2修改,3删除，4查询
            int flag=sc.nextInt();
            switch (flag){
                case 0:System.out.println("退出!");break;
                case 1:
                    System.out.println("请输入姓名");
                    name=sc.next();
                    System.out.println("学号");
                    studentId=sc.nextInt();
                    System.out.println("请输入年龄");
                    age=sc.nextInt();
                    Add.addStudent(name,studentId,age,listStudent);break;
                case 2:
                    SearchAll.allStudent(listStudent);
                    System.out.println("请输入姓名:");
                    name=sc.next();
                    System.out.println("请输入学号:");
                    studentId=sc.nextInt();
                    System.out.println("请输入年龄:");
                    age=sc.nextInt();
                    Student student=SearchSingle.searchSingleStudent(studentId,listStudent);
                    Update.updateStudent(name,studentId,age,student);
                    SearchAll.allStudent(listStudent);
                    break;
                case 3:
                    SearchAll.allStudent(listStudent);

                    System.out.println("请输入学号:"+studentId);

                    Delete.deleteStudent(studentId,listStudent);
                    SearchAll.allStudent(listStudent);
                    break;
                case 4:
                    System.out.println("请输入要查询的学号");
                    studentId=sc.nextInt();
                  Student stu=SearchSingle.searchSingleStudent(studentId,listStudent);
                  System.out.println("姓名:"+stu.getName()+"学号:"+stu.getStudentId()+"年龄:"+stu.getAge());
                    break;
            }
            if(flag==0){
                break;
            }
        }
        sc.close();
    }
}
